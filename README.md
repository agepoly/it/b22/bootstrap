### B22 cluster bootstrap

# Installing Argo CD

On a master node :
```bash
kubectl create ns argocd

helm repo add argo https://argoproj.github.io/argo-helm
helm install -n argocd argocd argo/argo-cd -f "https://gitlab.com/agepoly/it/b22/bootstrap/-/raw/main/argocd-values.yml"

kubectl apply -f "https://gitlab.com/agepoly/it/b22/bootstrap/-/raw/main/bootstrap-application.yml"
```

# Upgrading Argo CD

1. Update and push the new values
2. On a master node :
```bash
helm -n argocd upgrade argocd argo/argo-cd -f "https://gitlab.com/agepoly/it/b22/bootstrap/-/raw/main/argocd-values.yml"
```
